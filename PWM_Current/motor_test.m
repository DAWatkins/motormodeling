% Constants FILL THESE OUT AS APPROPRIATE

J = 1; %unity, fix
L = 0.00408; %inductance of motor
Rm = 1.33;
Rfet = 0.008;
R = Rm + 2*Rfet; %Resistance of motor and fets

Km = 0.119; %motor constant
Kb = 0.119; %back emf constant

V = 36; % dc input voltage
pwm = 0.75; %pwm duty cycle
f = 18000; %pwm frequency

Tfinal = 5; %sim time


% define system 

s = tf('s');

sys = 1 / (L*s + R) * 1 / (1 + Km*Kb / (L*J*s^2 + J*R*s^2));


% define input 
dt = 1/f; %pwm period

step = dt/100;

t = 0:step:Tfinal;


u = zeros(1,length(t));

for i = 1:1:length(t)
    if(mod(i*step,dt) <= dt*pwm)
        u(i) = 1;
    end
end

u = V * u;

lsim(sys, u, t)






