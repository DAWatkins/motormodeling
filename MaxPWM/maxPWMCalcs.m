% PURPOSE
% This script will calculate the PWM signal that corresponds to the maximum
% allowable current for a motor as a function of motor velocity.
% Calculated using an idealized motor model of constant internal resistance
% and backEMF constant

%performance parameters
minSpeed = 0;
maxSpeed = 5.5; %mph
speedStep = 0.05;
Rwheel = 3.8; %wheel radius, inches
maxPWMVal = 4000;

%motor specs
imax = 10; %maximum amps able to be supplied without failure
vmax = 40; %maximum voltage able to be supplied
rm = 0.643104; %armature resistance
km = 6.34; %emf motor constant rpm/v

%helper values and functions
mile2inch = 63360; 
ToRPM = @(mph) mph * mile2inch / 60 / (2 * pi * Rwheel); %conv mpm to rpm
calcVolt = @(rpm) rpm/km + imax*rm; %caculate voltage for given rpm

fs = 20; %font size
lw = 3; %line width


speeds = minSpeed:speedStep:maxSpeed;
rawVoltage = zeros(1,length(speeds));
for i = 1:1:length(speeds)
    rawVoltage(i) = calcVolt(ToRPM(speeds(i)));
end

clampedVoltage = min(rawVoltage, vmax);

pwmVals = floor(clampedVoltage * maxPWMVal / vmax);

figure();
ax = subplot(3,1,1);
plot(speeds, rawVoltage, 'LineWidth', lw);
title(['Raw Voltage @ ' num2str(imax) ' Amps']);
xlabel('Motor Speed (mph)');
ax.FontSize = fs;

ax = subplot(3,1,2);
plot(speeds, clampedVoltage, 'LineWidth', lw);
title('Voltage clamped to Battery Supply');
xlabel('Motor Speed (mph)');
ax.FontSize = fs;

ax = subplot(3,1,3);
plot(speeds, pwmVals, 'LineWidth', lw);
title('Maximum PWM');
xlabel('Motor Speed (mph)');
ax.FontSize = fs;